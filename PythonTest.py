import csv
import json
import logging


class Returner:
    """
    """

    def __init__(self, pathdata, filenamejson = 'jsonfile.json'):
        self.__logger = logging.getLogger('Returner logger')
        self.__logger.setLevel(logging.INFO)

        self.PATHDATA = pathdata
        self.DATA = []
        self.FILENAMEJSON = filenamejson

        self.getDataFromCSVFile()
    
    @property
    def filenamejson(self,):
        return self.FILENAMEJSON

    @filenamejson.setter
    def filenamejson(self, value):
        self.FILENAMEJSON = value    
    def getData(self,):
        return self.DATA
    def getDataFromCSVFile(self,):
        """
            Obtiene los datos desde un archivo CSV
        """
        try:
            with open(self.PATHDATA) as CSVDATA:
                objData = csv.DictReader(CSVDATA)
                self.DATA = [dict(i) for i in objData]
        except Exception as warninggetDataFromCSVFile:
            self.__logger.warning(f"Ocurrio una incidencia en la obtencion de datos :\
                 {warninggetDataFromCSVFile}")
    def conver2JSON(self,):
        """
            regresa un JSON con los valores de DATA y crea un archivo JSON
        """
        try:
            with open(self.FILENAMEJSON,'w') as jsonfile:
                jsonfile.write(json.dumps(self.DATA))
            
        except Exception as warningconver2JSON:
            self.__logger.warning(f"Ocurrio una incidencia en la creacion del json: \
                {warningconver2JSON}")
        else:
            return json.dumps(self.DATA)


def valuesFromHeader(header:str,data:list):
    """
        return values from passed header
    """
    dataheader = []
    try:
        for chunk in data:
            if isinstance(chunk, dict) and header in chunk:
                dataheader.append({header:chunk[header]})
            else:
                print(f"No es diccionario o el header no se encuentra en los datos {chunk}")
    except Exception as errorvaluesFromHeader:
        print(f"Ocurrio un error en retorno de valores por header {errorvaluesFromHeader}")
    else:
        return dataheader

        

if __name__ == "__main__":
   returner =Returner('data.csv')

   # 1.-Retornar todos los valores.
   print(returner.DATA)
   print(returner.getData())
   
   #2.-Crear una función que logre retornar por las cabeceras del archivo.
   print(valuesFromHeader('id',returner.DATA))

   #3.-Crea un método que de como salida un json con los valores que acabas de crear.
   print(returner.conver2JSON())

   
